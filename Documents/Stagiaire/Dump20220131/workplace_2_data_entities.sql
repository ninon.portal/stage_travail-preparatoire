-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: workplace_2
-- ------------------------------------------------------
-- Server version	5.7.36

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `data_entities`
--

DROP TABLE IF EXISTS `data_entities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `data_entities` (
  `id` int(10) unsigned NOT NULL,
  `external_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `name` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `privacy` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `purpose` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entity_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `insert_date_db` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_mcg` tinyint(1) NOT NULL DEFAULT '0',
  `network_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'workplace',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uidx_externalId` (`external_id`,`network_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_entities`
--

LOCK TABLES `data_entities` WRITE;
/*!40000 ALTER TABLE `data_entities` DISABLE KEYS */;
INSERT INTO `data_entities` VALUES (99054814,'99713507','4a822ae3cf3408ebc863cf72aff9b70d','King eagerly, and he went on for some time with.','public',NULL,'{\"archived\":false,\"cover\":\"\",\"description\":\"\"}','2020-07-27 06:04:33',NULL,'2022-01-31 14:54:15',0,'workplace'),(99298210,'99827447','a3155c6cd06fe33d0a927b03782e9b91','PLEASE mind what you\'re at!\" You know the.','public',NULL,'{\"archived\":false,\"cover\":\"\",\"description\":\"\"}','2020-09-15 22:02:41',NULL,'2022-01-31 14:54:15',0,'workplace'),(99330704,'99476322','09cd3f2eaa6be8527ada5d3aabaf03b3','Her first idea was that you never had fits, my.','public',NULL,'{\"archived\":false,\"cover\":\"\",\"description\":\"\"}','2021-09-16 13:47:10',NULL,'2022-01-31 14:54:15',0,'workplace');
/*!40000 ALTER TABLE `data_entities` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-31 16:07:05
